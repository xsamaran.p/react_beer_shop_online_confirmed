import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {Provider} from 'react-redux';

import {store} from './store/configureStore';
import {saveState} from './store/browser-storage';
import Shop from './features/shop/shop';
import Cart from './features/cart/cart';
import ItemDetails from './features/item/item-details/item-details';
import {NotFound} from './features/shared/components/error/not-found';
import {BasicLayout} from './features/layout/basic-layout';

store.subscribe(() => saveState(store.getState()));

const App = () =>
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<BasicLayout/>}>
            <Route index
              element={<Shop/>}
            />
            <Route path="shop" element={<Shop/>}/>
            <Route path="cart" element={<Cart/>}/>
            <Route path="details" element={<ItemDetails/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </React.StrictMode>
  </Provider>
;
export default App;
