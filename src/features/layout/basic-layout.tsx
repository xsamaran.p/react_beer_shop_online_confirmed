import React from 'react';
import Navigation from '../navbar';
import {Outlet} from 'react-router-dom';

export class BasicLayout extends React.Component {
  render() {
    return <>
      <Navigation/>
      <br/>
      <Outlet/>
    </>;
  }
}
