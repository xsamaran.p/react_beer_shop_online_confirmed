import {Container, Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

import siteLogo from '../asset/img/logo.svg';
import {useAppSelector} from '../store/hooks';
import {selectCartItemCount} from './cart/cart-slice';

const Navigation = () => {
  const cartItemCount = useAppSelector(selectCartItemCount);
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={NavLink} to={'../shop'}>
          <img
            alt="logo punk beer"
            src={siteLogo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          {' '}
          Beer Punk
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link id="shop-link" as={NavLink} to={'../shop'}>Shop</Nav.Link>
            <Nav.Link id="cart-link" as={NavLink} to={'../cart'}>
              Cart {cartItemCount !== 0 ? ` (${cartItemCount})` : ''}
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
