import {Amount} from '../models/amount';
import {Unit} from '../models/unit.model';

export function displayAmount(amount: Amount): string {
  let unit;
  switch (amount.unit) {
    case Unit.CELSUIS:
      unit = '°C';
      break;
    case Unit.KILOGRAMS:
      unit = 'kg';
      break;
    case Unit.GRAMS:
      unit = 'g';
      break;
    case Unit.LITER:
      unit = 'L';
      break;
  }
  return `${amount.value} ${unit}`;
}
