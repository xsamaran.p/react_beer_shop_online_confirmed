export enum Unit {
    CELSUIS = 'celsuis',
    LITER = 'litres',
    KILOGRAMS = 'kilograms',
    GRAMS = 'grams'
}
