import {ItemInfos} from './item-infos.type';

export type Item = { id: number, infos: ItemInfos, count: number }

