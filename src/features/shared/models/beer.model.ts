import {Amount} from './amount';
import {Ingredients} from './ingredient.model';
import {ItemType} from './item-type.enum';


interface Fermentation {
  temp: Amount;
}

interface MashTemp {
  temp: Amount;
  duration: null;
}

interface Method {
  fermentation: Fermentation;
  mash_temp: MashTemp[];
}

export interface Beer {
  type: ItemType.BEER;
  id: number;
  abv: number;
  boil_volume: Amount;
  brewers_tips: string;
  contributed_by: string;
  description: string;
  ebc: number;
  first_brewed: string;
  food_pairing: string[];
  ibu: number;
  image_url: string;
  ingredients: Ingredients;
  method: Method;
  name: string;
  ph: number;
  srm: number;
  tagline: string;
  target_fg: number;
  target_og: number;
  volume: Amount;
}
