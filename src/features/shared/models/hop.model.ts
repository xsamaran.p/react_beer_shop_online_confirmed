import {Amount} from './amount';

export interface Hop {
  add: string;
  amount: Amount;
  attribute: string;
  name: string;
}
