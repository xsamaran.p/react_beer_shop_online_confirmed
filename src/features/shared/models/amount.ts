import {Unit} from './unit.model';

export interface Amount {
  value: number,
  unit: Unit
}
