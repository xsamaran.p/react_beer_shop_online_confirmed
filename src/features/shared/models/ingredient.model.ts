import {Hop} from './hop.model';
import {Malt} from './malt.model';

export interface Ingredients {
  hops: Hop[];
  malt: Malt[];
  yeast: string;
}
