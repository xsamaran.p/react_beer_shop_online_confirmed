import {Component} from 'react';
import AbstractError from './abstractError';
import {Alert} from 'react-bootstrap';

export class FetchFailed extends
  Component<{ error: string | null }> {
  render() {
    const stringMessage = 'An error occured during the loading of the shop';
    const errorMsg: JSX.Element = !!this.props.error ?
            <>
              <hr/>
              <p>{this.props.error}</p>
            </> :
            <></>;

    const content: JSX.Element = <>
      {stringMessage}
      <Alert.Link href={'../shop'}>Please retry</Alert.Link>
      {errorMsg}
    </>;

    return <AbstractError variant={'danger'} content={content}/>;
  }
}
