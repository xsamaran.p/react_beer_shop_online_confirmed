import {Component} from 'react';
import AbstractError from './abstractError';
import {Alert} from 'react-bootstrap';

export class NotFound extends Component {
  render() {
    const stringMessage = 'Hmmmm how did you end up there ?';
    const content: JSX.Element = <>
      {stringMessage}
      <Alert.Link href={'./home'}> Go Back</Alert.Link>
    </>;

    return <AbstractError variant={'warning'} content={content}/>;
  }
}
