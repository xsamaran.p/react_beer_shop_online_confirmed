import {Component} from 'react';
import {Alert, Container} from 'react-bootstrap';
import {Variant} from 'react-bootstrap/types';

class AbstractError extends
  Component<{ content: JSX.Element, variant: Variant }> {
  render() {
    return <Container>
      <Alert style={{margin: '20vh', textAlign: 'center'}}
        variant={this.props.variant}>
        {this.props.content}
      </Alert>
    </Container>;
  }
}

export default AbstractError;
