import {Button, ButtonGroup} from 'react-bootstrap';

import {ItemInfos} from '../models/item-infos.type';
import {
  addCartItem,
  decreaseCartItemCount,
  increaseCartItemCount,
  removeCartItem,
  selectCartItem,
} from '../../cart/cart-slice';
import {Item} from '../models/cart-item.type';
import {useAppDispatch, useAppSelector} from '../../../store/hooks';
import {selectShopItemById} from '../../shop/shop-slice';

function ShopCartItemHandler(props: { itemId: number }) {
  const dispatch = useAppDispatch();
  const item: Item | undefined =
    useAppSelector((state) =>
      selectCartItem(state, props.itemId));
  const itemInfos: ItemInfos | undefined =
    useAppSelector((state) =>
      selectShopItemById(state, props.itemId));

  function addItemCliked() {
    if (!itemInfos) {
      return;
    }
    dispatch(addCartItem(itemInfos));
  }

  function decreaseItemCountClicked() {
    if (!item) {
      return;
    }
    dispatch(item.count === 1 ?
      removeCartItem(item) :
      decreaseCartItemCount(item));
  }

  function increaseItemCountClicked() {
    if (!item) {
      return;
    }
    dispatch(increaseCartItemCount(item));
  }

  return <>
    {!!item && item.count ?
      (
        <ButtonGroup>
          <Button variant="outline-primary"
            onClick={decreaseItemCountClicked}>-</Button>
          <Button disabled={true}>{item.count}</Button>
          <Button variant="outline-primary"
            onClick={increaseItemCountClicked}>+</Button>
        </ButtonGroup>
      ) : (
        <Button variant="outline-primary" onClick={addItemCliked}>
          Add to cart
        </Button>
      )
    }
  </>;
}


export default ShopCartItemHandler;
