import {Col, Container, Row} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';

import {useAppSelector} from '../../../store/hooks';
import {ItemInfos} from '../../shared/models/item-infos.type';
import ItemDetailsImg from './item-details-img';
import ItemDetailsTitle from './item-details-title';
import {selectItemSelected} from '../item-slice';
import ItemIngredientTabs from './item-details-ingredients-tabs';
import ItemDetailsAdditionalInfos from './item-details-additional-infos';
import ItemDescription from './item-description';
import {ItemType} from '../../shared/models/item-type.enum';
import BeerDetailsColor from './beer-details/beer-details-color';
import FoodPairing from './beer-details/food-pairing';

const ItemDetails = () => {
  const itemInfos: ItemInfos | undefined = useAppSelector(selectItemSelected);

  return <>
    {!itemInfos ? <Navigate to={'../shop'} replace={false}/> : (
      <Container>
        <Row>
          <Col><ItemDetailsImg/></Col>
          <Col><ItemDetailsTitle/></Col>
        </Row>
        <hr/>
        <Row>
          <ItemDescription/>
        </Row>
        <hr/>
        <Row>
          <Col md={{span: 3}}>
            <ItemDetailsAdditionalInfos/>
          </Col>
          <Col md={{span: 3, offset: 1}}>
            {(itemInfos.type === ItemType.BEER) ? <BeerDetailsColor/> : <></>}
          </Col>
          <hr/>
          <Col md={{span: 4}}>
            <FoodPairing/>
          </Col>
          <Col md={{span: 8}}>
            <h3>Brewers tips :</h3>
            <p>{itemInfos.brewers_tips}</p>
          </Col>
          <hr/>
          <Col md={{span: 6}}>
            <ItemIngredientTabs/>
          </Col>
        </Row>
      </Container>
    )}
  </>;
};

export default ItemDetails;
