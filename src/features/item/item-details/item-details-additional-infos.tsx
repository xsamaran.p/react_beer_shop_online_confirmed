import {useAppSelector} from '../../../store/hooks';
import {selectItemSelected} from '../item-slice';
import {Table} from 'react-bootstrap';
import {ItemInfos} from '../../shared/models/item-infos.type';

const ItemDetailsAdditionalInfos = () => {
  const itemInfos: ItemInfos | undefined = useAppSelector(selectItemSelected);

  if (!itemInfos) {
    return <></>;
  }

  return <>
    <h2>Additional Infos</h2>
    <Table bordered={true}>
      <thead>
        <tr>
          <th>Name</th>
          <th>Unit</th>
        </tr>
      </thead>
      <tbody>
        <tr key={`tr-ibu-${itemInfos.ibu}`}>
          <td key={`td-ibu-title`}>Bitterness</td>
          <td key={`td-ibu-${itemInfos.abv}`}>{itemInfos.ibu}/100</td>
        </tr>
        <tr key={`tr-abv-${itemInfos.abv}`}>
          <td key={`td-abv-title`}>Alcool by volume</td>
          <td key={`td-abv-${itemInfos.abv}`}>{itemInfos.abv} %</td>
        </tr>
      </tbody>
    </Table>
  </>;
};

export default ItemDetailsAdditionalInfos;
