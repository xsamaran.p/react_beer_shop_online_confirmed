import {Col, Row} from 'react-bootstrap';

import {ItemInfos} from '../../shared/models/item-infos.type';
import {useAppSelector} from '../../../store/hooks';
import ShopCartItemHandler
  from '../../shared/components/shop-cart-item-handler';
import ItemDetailsVolume from './item-details-volume';
import {selectItemSelected} from '../item-slice';


const ItemDetailsTitle = () => {
  const detailsInfos: ItemInfos | undefined =
    useAppSelector(selectItemSelected);
  if (!detailsInfos) {
    return null;
  }
  return <>
    <h1>{detailsInfos.name}</h1>
    <h3>{detailsInfos.tagline}</h3>
    <Row>
      <Col md={{span: 3}}>
        <ItemDetailsVolume/>
      </Col>
      <Col md={{span: 3, offset: 6}}>
        <ShopCartItemHandler itemId={detailsInfos.id}/>
      </Col>
    </Row>
  </>;
};

export default ItemDetailsTitle;
