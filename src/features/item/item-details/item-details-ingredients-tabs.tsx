import {Tab, Tabs} from 'react-bootstrap';

import MaltTable from './beer-details/malt-table';
import HopsTable from './beer-details/hops-table';
import {useAppSelector} from '../../../store/hooks';
import {selectItemSelectedIngredientsYeast} from '../item-slice';

const ItemIngredientTabs = () => {
  const yeast: string | undefined =
    useAppSelector(selectItemSelectedIngredientsYeast);

  return <>
    <h2>Ingredients</h2>
    <Tabs defaultActiveKey="malts" id="ingredient-tab" className="mb-6">
      <Tab eventKey="malts" title="Malts" key={'malts-tab'}>
        <MaltTable/>
      </Tab>
      <Tab eventKey="hops" title="Hops" key={'hops-tab'}>
        <HopsTable/>
      </Tab>
      <Tab eventKey="yeast" title="Yeast" key={'yeast-tab'}>
        {!yeast ? <></> : <span> Name : {yeast}</span>}
      </Tab>
    </Tabs>
  </>;
};

export default ItemIngredientTabs;
