import {ItemInfos} from '../../shared/models/item-infos.type';
import {useAppSelector} from '../../../store/hooks';
import {selectItemSelected} from '../item-slice';

const ItemDescription = () => {
  const itemInfos: ItemInfos | undefined = useAppSelector(selectItemSelected);

  if (!itemInfos) {
    return <></>;
  }

  return <>
    <h2>Descritpion</h2>
    <p>{itemInfos.description}</p>
    <h6>First brewed: {itemInfos.first_brewed}</h6>
  </>;
};

export default ItemDescription;
