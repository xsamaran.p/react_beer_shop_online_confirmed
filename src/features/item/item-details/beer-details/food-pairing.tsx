import {Table} from 'react-bootstrap';

import {useAppSelector} from '../../../../store/hooks';
import {selectItemSelectedFoodPairing} from '../../item-slice';

const FoodPairing = () => {
  const foodPairing: string[] | undefined =
    useAppSelector(selectItemSelectedFoodPairing);

  if (!foodPairing) {
    return <></>;
  }

  return <>
    <Table bordered={true}>
      <thead>
        <tr>
          <th>Food Pairing</th>
        </tr>
      </thead>
      <tbody>
        {foodPairing.map((i) => <tr key={`tr-${i}`}>
          <td key={`td-foodPairing-${i}`}>{i}</td>
        </tr>)}
      </tbody>
    </Table>
  </>;
};
export default FoodPairing;
