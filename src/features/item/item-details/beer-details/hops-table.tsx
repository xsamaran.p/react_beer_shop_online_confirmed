import {Table} from 'react-bootstrap';

import {displayAmount} from '../../../shared/utils/display.utils';
import {Hop} from '../../../shared/models/hop.model';
import {useAppSelector} from '../../../../store/hooks';
import {selectItemSelectedIngredientsHops} from '../../item-slice';

const HopsTable = () => {
  const hops: Hop[] | undefined =
    useAppSelector(selectItemSelectedIngredientsHops);

  if (!hops) {
    return <></>;
  }

  return <Table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Adding time</th>
        <th>Amount</th>
        <th>Attribute</th>
      </tr>
    </thead>
    <tbody>
      {hops.map((i) => <tr key={`tr-hops-line-${hops.indexOf(i)}`}>
        <td key={`td-hops-${hops.indexOf(i)}-${i.name}`}>{i.name}</td>
        <td key={`td-hops-${hops.indexOf(i)}-${i.add}`}>{i.add}</td>
        <td key={`td-hops-${hops.indexOf(i)}-${i.amount.value}`}>
          {displayAmount(i.amount)}
        </td>
        <td key={`td-hops-${hops.indexOf(i)}-${i.attribute}`}>{i.attribute}</td>
      </tr>)}
    </tbody>
  </Table>;
};

export default HopsTable;
