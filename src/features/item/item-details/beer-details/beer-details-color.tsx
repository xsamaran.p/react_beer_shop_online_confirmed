import {useAppSelector} from '../../../../store/hooks';
import {selectItemSelectedEBC} from '../../item-slice';
import {Col, Row} from 'react-bootstrap';

const BeerDetailsColor = () => {
  const ebc = useAppSelector(selectItemSelectedEBC);
  const backgroundColor = [
    {
      color: '#d8dc53',
      name: 'Pale lager, Witbier, Pilsener, Berliner Weisse',
    }, {
      color: '#D5BC26',
      name: 'Weissbier',
    }, {
      color: '#BF923B',
      name: 'American Pale Ale, India Pale Ale',
    }, {
      color: '#BF813A',
      name: 'Weissbier, Saison',
    }, {
      color: '#BC6733',
      name: 'English Bitter, ESB',
    }, {
      color: '#8D4C32',
      name: 'Bière de Garde, Double IPA',
    }, {
      color: '#5D341A',
      name: 'Dark lager, Vienna lager, Märzen, Amber Ale',
    }, {
      color: '#261716',
      name: 'Brown Ale, Bock, Dunkel, Dunkelweizen',
    }, {
      color: '#0F0B0A',
      name: 'Irish Dry Stout, Doppelbock, Porter',
    }];
  let variant: { color: string, name: string };


  if (!ebc) {
    variant = {color: 'red', name: 'error'};
  } else if (ebc < 8) {
    variant = backgroundColor[0];
  } else if (ebc < 14) {
    variant = backgroundColor[1];
  } else if (ebc < 18) {
    variant = backgroundColor[2];
  } else if (ebc < 22) {
    variant = backgroundColor[3];
  } else if (ebc < 29) {
    variant = backgroundColor[4];
  } else if (ebc < 36) {
    variant = backgroundColor[5];
  } else if (ebc < 43) {
    variant = backgroundColor[6];
  } else if (ebc < 54) {
    variant = backgroundColor[7];
  } else {
    variant = backgroundColor[8];
  }


  return <>
    <h2>Beer color : </h2>
    <Row>
      {backgroundColor.map((i) => <Col
        key={`beer-color-${i.name}`}
        title={i.name}
        style={{
          margin: 0,
          height: '7rem',
          width: '3rem',
          backgroundColor: i.color,
          border: variant.color === i.color ? ('red solid') : 'none',
        }}
      />)}
    </Row>
  </>;
};

export default BeerDetailsColor;
