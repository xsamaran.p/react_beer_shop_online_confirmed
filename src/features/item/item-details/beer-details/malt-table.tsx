import {Table} from 'react-bootstrap';

import {displayAmount} from '../../../shared/utils/display.utils';
import {Malt} from '../../../shared/models/malt.model';
import {useAppSelector} from '../../../../store/hooks';
import {selectItemSelectedIngredientsMalts} from '../../item-slice';

const MaltTable = () => {
  const malts: Malt[] | undefined =
    useAppSelector(selectItemSelectedIngredientsMalts);

  if (!malts) {
    return <></>;
  }

  return <Table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>
      {malts.map((i) => <tr key={`tr-malt-${malts.indexOf(i)}-${i.name}`}>
        <td key={`td-malt-${malts.indexOf(i)}-${i.name}`}>{i.name}</td>
        <td key={`td-malt-${malts.indexOf(i)}-${displayAmount(i.amount)}`}>
          {displayAmount(i.amount)}
        </td>
      </tr>)}
    </tbody>
  </Table>;
};

export default MaltTable;
