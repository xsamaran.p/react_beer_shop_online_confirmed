import {Figure} from 'react-bootstrap';

import {useAppSelector} from '../../../store/hooks';
import {selectItemSelectedImg} from '../item-slice';

function ItemDetailsImg() {
  const itemImage: string | undefined =
    useAppSelector((state) => selectItemSelectedImg(state));
  return <Figure.Image height={'50%'} width={'25%'} src={itemImage}/>;
}

export default ItemDetailsImg;
