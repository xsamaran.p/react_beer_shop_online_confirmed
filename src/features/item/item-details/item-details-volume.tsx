import {ItemInfos} from '../../shared/models/item-infos.type';
import {useAppSelector} from '../../../store/hooks';
import {displayAmount} from '../../shared/utils/display.utils';
import {selectItemSelected} from '../item-slice';

const ItemDetailsVolume = () => {
  const itemInfos: ItemInfos | undefined = useAppSelector(selectItemSelected);
  if (!itemInfos) {
    return null;
  }
  return <h3>{displayAmount(itemInfos.volume)}</h3>;
};

export default ItemDetailsVolume;
