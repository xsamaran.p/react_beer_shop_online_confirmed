import {createSlice, PayloadAction} from '@reduxjs/toolkit';

import {ItemInfos} from '../shared/models/item-infos.type';
import {RootState} from '../../store/configureStore';

interface ShopState {
  selected?: ItemInfos;
}


const initialState: ShopState = {
  selected: undefined,
};

export const itemSlice = createSlice({
  name: 'item',
  initialState: initialState,
  reducers: {
    itemSelect: (state, action: PayloadAction<ItemInfos>) => {
      state.selected = action.payload;
    },

  },
});


export const {itemSelect} = itemSlice.actions;

export const selectItemSelected = (state: RootState) =>
  state.item.selected;
export const selectItemSelectedEBC = (state: RootState) =>
  state.item.selected?.ebc;
export const selectItemSelectedFoodPairing = (state: RootState) =>
  state.item.selected?.food_pairing;
export const selectItemSelectedImg = (state: RootState) =>
  state.item.selected?.image_url;
export const selectItemSelectedIngredientsMalts = (state: RootState) =>
  state.item.selected?.ingredients.malt;
export const selectItemSelectedIngredientsHops = (state: RootState) =>
  state.item.selected?.ingredients.hops;
export const selectItemSelectedIngredientsYeast = (state: RootState) =>
  state.item.selected?.ingredients.yeast;


export default itemSlice.reducer;
