import {useEffect} from 'react';
import {Container, Row} from 'react-bootstrap';
import ShopItemCard from './shop-item-card/shop-item-card';
import {useAppDispatch, useAppSelector} from '../../../store/hooks';
import {DataStatus} from '../../shared/models/status.enum';
import ShopItemCardPlaceholder
  from './shop-item-card/shop-item-card-placeholder';


import {
  fetchShopItems,
  selectShopItemPage,
  selectShopItemPerPage,
  selectShopItemsIdList,
  selectShopStatus,
} from '../shop-slice';
import {FetchFailed} from '../../shared/components/error/fetch-failed';

const ShopItemList = () => {
  const dispatch = useAppDispatch();
  const itemsId: number[] = useAppSelector(selectShopItemsIdList);

  const shopItemPage = useAppSelector(selectShopItemPage);
  const shopItemPerPage = useAppSelector(selectShopItemPerPage);
  const shopStatus = useAppSelector(selectShopStatus);
  const error = useAppSelector((state) => state.shop.error);

  useEffect(() => {
    if (shopStatus === DataStatus.IDLE) {
      dispatch(fetchShopItems({page: shopItemPage, per_page: shopItemPerPage}));
    }
  }, [shopItemPage, shopItemPerPage, shopStatus, dispatch]);

  let content;

  if (shopStatus === DataStatus.LOADING) {
    content = Array.from(Array(shopItemPerPage).keys()).map((i) =>
      <ShopItemCardPlaceholder key={i}/>);
  } else if (shopStatus === DataStatus.SUCCEEDED) {
    content = <>
      {itemsId.map((itemId) => {
        return <ShopItemCard key={itemId} itemId={itemId}/>;
      })}
    </>;
  } else if (shopStatus === DataStatus.FAILED) {
    content = <FetchFailed error={error}/>;
  }

  return <>
    <Container>
      <Row xs={1} md={2} className="row-cols-lg-auto">
        {content}
      </Row>
    </Container>
  </>;
};

export default ShopItemList;
