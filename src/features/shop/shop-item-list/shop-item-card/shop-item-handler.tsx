import {ButtonGroup, ButtonToolbar} from 'react-bootstrap';

import ShopItemVolume from './shop-item-volume';
import ShopCartItemHandler
  from '../../../shared/components/shop-cart-item-handler';

const ShopItemHandler = (props: { itemId: number }) => <>
  <ButtonToolbar
    style={{'paddingBottom': '10px'}}
    className="justify-content-between">
    <ButtonGroup>
      <ShopItemVolume itemId={props.itemId}/>
    </ButtonGroup>
    <ButtonGroup>
      <ShopCartItemHandler itemId={props.itemId}/>
    </ButtonGroup>
  </ButtonToolbar>
</>;

export default ShopItemHandler;
