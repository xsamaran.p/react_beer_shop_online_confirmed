import {Card} from 'react-bootstrap';

import ShopItemHandler from './shop-item-handler';
import {ItemInfos} from '../../../shared/models/item-infos.type';
import {useAppSelector} from '../../../../store/hooks';
import {selectShopItemById} from '../../shop-slice';
import ShopItemImg from './shop-item-img';

const ShopItemCard = (props: { itemId: number }) => {
  const itemInfos: ItemInfos | undefined =
    useAppSelector((state) =>
      selectShopItemById(state, props.itemId));

  return <>
    <Card style={{width: '13rem', margin: '10px'}}
      key={itemInfos?.id}
      bg={'light'}
      border={'dark'}>
      <ShopItemImg itemId={props.itemId}/>
      <Card.Body>
        <Card.Title>{itemInfos?.name}</Card.Title>
        <Card.Text>{itemInfos?.tagline}</Card.Text>
      </Card.Body>
      <ShopItemHandler itemId={props.itemId}/>
    </Card>
  </>;
};

export default ShopItemCard;
