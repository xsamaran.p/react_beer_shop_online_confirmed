import {Card, Placeholder} from 'react-bootstrap';

const ShopItemCardPlaceholder = () => <>
  <Card style={{width: '18rem', margin: '10px'}} bg={'light'}
    border={'dark'}>
    <Card.Img variant="top" src="https://via.placeholder.com/100"/>
    <Card.Body>
      <Placeholder as={Card.Title} animation="glow">
        <Placeholder xs={6}/>
      </Placeholder>
      <Placeholder as={Card.Text} animation="glow">
        <Placeholder xs={6}/>
        <Placeholder xs={4}/>
        <Placeholder xs={4}/>
        {' '}
        <Placeholder xs={6}/>
        <Placeholder xs={8}/>
      </Placeholder>
    </Card.Body>
  </Card>
</>;
export default ShopItemCardPlaceholder;
