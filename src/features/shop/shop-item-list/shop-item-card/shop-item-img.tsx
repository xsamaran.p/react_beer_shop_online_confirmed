import {useNavigate} from 'react-router-dom';
import {Card, Col, Figure, Row} from 'react-bootstrap';

import {ItemInfos} from '../../../shared/models/item-infos.type';
import {selectShopItemById} from '../../shop-slice';
import {useAppDispatch, useAppSelector} from '../../../../store/hooks';
import {itemSelect} from '../../../item/item-slice';


const ShopItemImg = (props: { itemId: number }) => {
  const itemInfos: ItemInfos | undefined =
    useAppSelector((state) =>
      selectShopItemById(state, props.itemId));
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  if (!itemInfos) {
    return <></>;
  }

  const goToDetailsClick = () => {
    if (!itemInfos) {
      return;
    }
    dispatch(itemSelect(itemInfos));
    navigate('../details', {replace: false});
  };

  let content;
  if (!itemInfos.image_url) {
    content = <Card.Img variant="top"
      src="https://via.placeholder.com/700?text=Image+is+missing"/>;
  } else if (itemInfos.image_url.includes('keg')) {
    content = <Col md={{span: 6, offset: 3}}>

      <Figure.Image style={{cursor: 'pointer'}}
        title={'Click to go to details'}
        src={itemInfos?.image_url}
        onClick={goToDetailsClick}
      />
    </Col>;
  } else {
    content = <Col style={{'marginLeft': '4rem'}} md={{span: 12}}>
      <Figure.Image style={{cursor: 'pointer'}}
        title={'Click to go to details'}
        height={'50%'}
        width={'25%'}
        src={itemInfos.image_url}
        onClick={goToDetailsClick}
      />
    </Col>;
  }

  return <>
    <br/>
    <Row>
      {content}
    </Row>
  </>;
};

export default ShopItemImg;
