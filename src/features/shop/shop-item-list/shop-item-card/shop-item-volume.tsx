import {ItemInfos} from '../../../shared/models/item-infos.type';
import {useAppSelector} from '../../../../store/hooks';
import {selectShopItemById} from '../../shop-slice';
import {displayAmount} from '../../../shared/utils/display.utils';

const ShopItemVolume = (props: { itemId: number }) => {
  const itemInfos: ItemInfos | undefined =
    useAppSelector((state) =>
      selectShopItemById(state, props.itemId));

  if (!itemInfos) {
    return null;
  }
  return <h5
    style={{'marginTop': '0.5rem'}}>{displayAmount(itemInfos.volume)}</h5>;
};

export default ShopItemVolume;
