import {Button, ButtonGroup, Col, Row} from 'react-bootstrap';

import {useAppDispatch, useAppSelector} from '../../../store/hooks';
import {
  loadItemPage,
  selectShopItemNumber,
  selectShopItemPage,
  selectShopItemPerPage,
  selectShopStatus,
} from '../shop-slice';
import {DataStatus} from '../../shared/models/status.enum';

const LoadMoreShopItem = () => {
  const dispatch = useAppDispatch();
  const shopPage: number = useAppSelector(selectShopItemPage);
  const shopPerPage: number = useAppSelector(selectShopItemPerPage);
  const shopItemNumber: number = useAppSelector(selectShopItemNumber);
  const shopStatus: DataStatus = useAppSelector(selectShopStatus);

  const loadPreviousItemPageClicked = () => {
    dispatch(loadItemPage(shopPage - 1));
  };

  const loadNextItemPageClicked = () => {
    dispatch(loadItemPage(shopPage + 1));
  };

  return <>
    <Row>
      <Col md={{span: 4, offset: 4}}>
        <ButtonGroup>
          <Button variant={'primary'}
            disabled={shopStatus === DataStatus.LOADING || shopPage === 1}
            onClick={loadPreviousItemPageClicked}>
            {shopStatus === DataStatus.LOADING ? 'Loading...' : 'Previous page'}
          </Button>
          <Button variant={'outline-secondary'}
            disabled={true}>{shopPage}</Button>
          <Button variant={'primary'}
            disabled={shopStatus === DataStatus.LOADING ||
                    shopPerPage > shopItemNumber}
            onClick={loadNextItemPageClicked}>
            {shopStatus === DataStatus.LOADING ? 'Loading...' : 'Next page'}
          </Button>
        </ButtonGroup>
      </Col>
    </Row>
  </>;
};
export default LoadMoreShopItem;
