import axios, {AxiosResponse} from 'axios';

import {Beer} from '../../shared/models/beer.model';
import {ItemType} from '../../shared/models/item-type.enum';

const BeerService = {

  axios: axios.create({
    baseURL: 'https://api.punkapi.com/v2/',
    timeout: 1000,
  }),

  getBeers(page?: number, itemLimit?: number): Promise<AxiosResponse<Beer[]>> {
    return this.axios.get<Beer[]>(
        `/beers`,
        {
          params: {
            page: page,
            per_page: itemLimit,
          },
        })
        .then((axiosResponse) => {
          axiosResponse.data = axiosResponse.data.map((d: Beer) => {
            return {...d, type: ItemType.BEER};
          });
          return axiosResponse;
        });
  },
};
export default BeerService;
