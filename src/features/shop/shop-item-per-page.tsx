import {ChangeEvent} from 'react';
import {Col, Form, Row} from 'react-bootstrap';

import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {changeItemPerPage, selectShopItemPerPage} from './shop-slice';

const ShopItemPerPage = () => {
  const itemPerPage = useAppSelector(selectShopItemPerPage);

  const dispatch = useAppDispatch();

  const switchItemPerPageClicked = (event: ChangeEvent<HTMLSelectElement>) => {
    dispatch(changeItemPerPage(Number(event.target.value)));
  };

  return <Row>
    <Col md={{span: 3, offset: 9}}>
      <div><span>Item per page :</span>
        <Form.Select aria-label="item-per-page" value={itemPerPage}
          onChange={(event) => switchItemPerPageClicked(event)}>
          {Array.from(Array(16)
              .keys())
              .map((i) =>
                <option key={(i + 1) * 5} value={(i + 1) * 5}>
                  {(i + 1) * 5}
                </option>,
              )
          }
        </Form.Select></div>
    </Col>
  </Row>;
};

export default ShopItemPerPage;
