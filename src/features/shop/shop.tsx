import {Container} from 'react-bootstrap';

import ShopItemList from './shop-item-list/shop-item-list';
import ShopItemPerPage from './shop-item-per-page';
import LoadMoreShopItem from './shop-item-list/load-more-shop-item';

const Shop = () => <Container id={'shop'}>
  <ShopItemPerPage/>
  <ShopItemList/>
  <br/>
  <LoadMoreShopItem/>
  <br/>
</Container>;

export default Shop;
