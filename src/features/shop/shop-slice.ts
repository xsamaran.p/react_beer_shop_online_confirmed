import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

import {ItemInfos} from '../shared/models/item-infos.type';
import {RootState} from '../../store/configureStore';
import beerService from './services/beer.service';
import {DataStatus} from '../shared/models/status.enum';

interface ShopState {
  itemsList: ItemInfos[],
  per_page: number,
  page: number,
  status: DataStatus,
  error: string | null
}


const initialState: ShopState = {
  itemsList: [],
  page: 1,
  per_page: 10,
  status: DataStatus.IDLE,
  error: null,
};

export const shopSlice = createSlice({
  name: 'shop',
  initialState: initialState,
  reducers: {
    shopChangeItemPerPage: (state,
        action: PayloadAction<number>) => {
      state.per_page = action.payload;
    },
    shopChangeItemPage: (state,
        action: PayloadAction<number>) => {
      state.page = action.payload;
    },
  },
  extraReducers(builder) {
    builder
        .addCase(fetchShopItems.pending, (state) => {
          state.status = DataStatus.LOADING;
        })
        .addCase(fetchShopItems.fulfilled, (state,
            action: PayloadAction<ItemInfos[]>) => {
          state.status = DataStatus.SUCCEEDED;
          state.itemsList = action.payload;
        })
        .addCase(fetchShopItems.rejected, (state) => {
          state.status = DataStatus.FAILED;
        });
  },
});

export const changeItemPerPage =
  createAsyncThunk<void, number, { state: RootState }>(
      'shop/changeItemPerPage',
      async (newNumber: number, {getState, dispatch}) => {
        dispatch(fetchShopItems({
          page: getState().shop.page,
          per_page: newNumber,
        }));
        dispatch(shopSlice.actions.shopChangeItemPerPage(newNumber));
      });

export const loadItemPage =
  createAsyncThunk<void, number, { state: RootState }>(
      'shop/loadPreviousItemPage',
      async (page: number, {getState, dispatch}) => {
        dispatch(shopSlice.actions.shopChangeItemPage(page));
        dispatch(fetchShopItems({
          page: page,
          per_page: getState().shop.per_page,
        }));
      });

export const fetchShopItems =
  createAsyncThunk<ItemInfos[], { page: number, per_page: number }>(
      'shop/fetchShopItems',
      async (query: { page: number, per_page: number }) => {
        const response = await beerService.getBeers(query.page, query.per_page);
        return response.data;
      });

export const selectShopStatus = (state: RootState) =>
  state.shop.status;
export const selectShopItemPage = (state: RootState) =>
  state.shop.page;
export const selectShopItemPerPage = (state: RootState) =>
  state.shop.per_page;
export const selectShopItemNumber = (state: RootState) =>
  state.shop.itemsList.length;
export const selectShopItemsIdList = (state: RootState) =>
  state.shop.itemsList.map((item) => item.id);
export const selectShopItemById = (state: RootState, id: number) =>
  state.shop.itemsList.find((item) => item.id === id);

export default shopSlice.reducer;
