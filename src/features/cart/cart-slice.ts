import {createSlice, Draft, PayloadAction} from '@reduxjs/toolkit';

import {Item} from '../shared/models/cart-item.type';
import {ItemInfos} from '../shared/models/item-infos.type';
import {RootState} from '../../store/configureStore';

interface CartState {
  items: Item[];
}

const initialState: CartState = {
  items: [],
};
export const cartSlice = createSlice({
  name: 'cart',
  initialState: initialState,
  reducers: {
    addCartItem: (
        state: Draft<CartState>,
        action: PayloadAction<ItemInfos>) => {
      state.items.push({
        id: action.payload.id,
        infos: action.payload,
        count: 1,
      });
    },
    increaseCartItemCount: (
        state,
        action: PayloadAction<Item>) => {
      state
          .items[state.items.findIndex((item) => item.id === action.payload.id)]
          .count += 1;
    },
    decreaseCartItemCount: (
        state,
        action: PayloadAction<Item>) => {
      state
          .items[state.items.findIndex((item) => item.id === action.payload.id)]
          .count -= 1;
    },
    removeCartItem: (
        state,
        action: PayloadAction<Item>) => {
      state.items.splice(state.items.indexOf(action.payload));
    },

  },
});

export const {
  addCartItem,
  removeCartItem,
  increaseCartItemCount,
  decreaseCartItemCount,
} = cartSlice.actions;

export const selectCartItem = (state: RootState, itemId: number) =>
  state.cart.items.find((item) => item.id === itemId);
export const selectCartItemInfos = (state: RootState, itemId: number) =>
  state.cart.items.find((item) => item.id === itemId)?.infos;
export const selectCartItemCount = (state: RootState) =>
  state.cart.items.length === 0 ?
    0 :
    state.cart.items.map((item) => item.count).reduce((c1, c2) => c1 + c2);
export const selectCart = (state: RootState) =>
  state.cart.items.map((item) => item.id);

export default cartSlice.reducer;
