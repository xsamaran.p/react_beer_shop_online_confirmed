import {ItemInfos} from '../shared/models/item-infos.type';
import {useAppSelector} from '../../store/hooks';
import {selectCartItemInfos} from './cart-slice';
import {Col, Row} from 'react-bootstrap';
import ShopCartItemHandler from '../shared/components/shop-cart-item-handler';
import CartItemImage from './cart-item-image';

const CartItem = (props: { itemId: number }) => {
  const cartItem: ItemInfos | undefined =
    useAppSelector((state) =>
      selectCartItemInfos(state, props.itemId));

  if (!cartItem) {
    return null;
  }
  return <>
    <Col md={{span: 3}}>
      <Row>
        <CartItemImage itemId={cartItem.id}/>
      </Row>
    </Col>
    <Col md={{span: 6}}>
      <Row>
        <Col>
          <h1>{cartItem.name}</h1>
          <p>{cartItem.tagline}</p>
        </Col>
      </Row>
    </Col>
    <Col md={{span: 3}}>
      <ShopCartItemHandler itemId={cartItem.id}/>
    </Col>
    <hr/>
  </>;
};

export default CartItem;
