import {Row} from 'react-bootstrap';

import {useAppSelector} from '../../store/hooks';
import {selectCart} from './cart-slice';
import CartItem from './cart-item';

const Cart = () => {
  const cartItemId = useAppSelector(selectCart);
  return (
    <>
      <hr/>
      {cartItemId.map((id) =>
        <Row key={id}><CartItem itemId={id}/></Row>,
      )}
    </>
  );
};

export default Cart;
