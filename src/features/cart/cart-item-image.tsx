import {useNavigate} from 'react-router-dom';
import {Card, Col, Figure} from 'react-bootstrap';
import {ItemInfos} from '../shared/models/item-infos.type';
import {useAppDispatch, useAppSelector} from '../../store/hooks';
import {selectShopItemById} from '../shop/shop-slice';
import {itemSelect} from '../item/item-slice';


function CartItemImage(props: { itemId: number }) {
  const itemInfos: ItemInfos | undefined =
    useAppSelector((state) =>
      selectShopItemById(state, props.itemId));
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const goToDetailsClick = () => {
    if (!itemInfos) {
      return;
    }
    dispatch(itemSelect(itemInfos));
    navigate('../details', {replace: false});
  };
  if (!itemInfos) {
    return <></>;
  }
  if (!itemInfos.image_url) {
    return <Card.Img variant="top"
      src="https://via.placeholder.com/700?text=Image+is+missing"/>;
  } else if (itemInfos.image_url.includes('keg')) {
    return <Col md={{span: 3, offset: 4}}>
      <Figure.Image style={{cursor: 'pointer'}}
        title={'Click for details'}
        src={itemInfos?.image_url}
        onClick={goToDetailsClick}
      />
    </Col>;
  } else {
    return <Col md={{span: 2, offset: 4}}>
      <Figure.Image style={{cursor: 'pointer', marginLeft: '1rem'}}
        title={'Click for details'}
        src={itemInfos.image_url}
        onClick={goToDetailsClick}
      />
    </Col>;
  }
}

export default CartItemImage;
