import {configureStore} from '@reduxjs/toolkit';

import cartSlice from '../features/cart/cart-slice';
import shopSlice from '../features/shop/shop-slice';
import itemSlice from '../features/item/item-slice';
import {loadState} from './browser-storage';

export const store = configureStore({
  reducer: {
    cart: cartSlice,
    shop: shopSlice,
    item: itemSlice,
  },
  preloadedState: loadState(),
});

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
